# projectHTML

Projet HTML réalisé au cours de ma formation en autodidacte avec
la plateforme Dyma.fr 

Le premier projet concerne un site vitrine uniquement en HTML/CSS
pour un café.

Le deuxième projet concerne un site vitrine pour une agence
immobilière uniquement en HTML/CSS avec les notions de flexbox pour la gestion du CSS et du responsive.

Le troisième projet est une HomePage HTML d'un journal en ligne le tout réalisé avec de fausses data et aussi responsive sur mobile avec les display grid et flexbox

Le quatrième projet est une Page CV faite en HTML CSS et SASS responsive sur mobile. 
